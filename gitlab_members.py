import argparse
import csv
import time
import math
import datetime
from utils import get_gitlab

parser = argparse.ArgumentParser('gitlab_members')
parser.add_argument('id', help='Gitlab group id', type=str)
parser.add_argument('token', help='Gitlab access token', type=str)
parser.add_argument('--admin_only', action='store_true',
                    help='Show only members with admin permissions')
args = parser.parse_args()


def get_members_info(members, kind, id_, name):
    info = []
    for m in members:
        if args.admin_only and m.access_level < 40:
            continue

        created_by = (m.created_by.get('username') if hasattr(m, 'created_by')
                      else '[unknown]')
        # https://docs.gitlab.com/ee/api/members.html
        access_level = {
            0: 'No access',
            5: 'Minimal access',
            10: 'Guest',
            20: 'Reporter',
            30: 'Developer',
            40: 'Maintainer',
            50: 'Owner',
        }[m.access_level]
        info.append({
            'kind': kind,
            'id': id_,
            'name': name,
            'member': m.username,
            'level': access_level,
            'granted_by': created_by,
        })
    return info


def get_group_info(gl, id_):
    group = gl.groups.get(id_)
    members = group.members.list(get_all=True)
    return get_members_info(members, 'group', id_, group.name)


start_time = time.time()
print('authenticating to Gitlab...')
gl = get_gitlab(args.token)

print(f'getting data for the {args.id} group...')
main_group = gl.groups.get(args.id)
seen_groups = [main_group.id]
result = get_group_info(gl, main_group.id)

print(f'getting projects data for the {main_group.name} group...')
group_projects = main_group.projects.list(get_all=True, include_subgroups=True)

eta = (f'{len(group_projects)} sec' if len(group_projects) < 60
       else f'{math.ceil(len(group_projects) / 60)} min')
print(f'analysing {len(group_projects)} project(s). This may take up to {eta}')
for group_project in group_projects:
    project = gl.projects.get(group_project.id)
    # Skipping shared projects from other groups
    if len(project.shared_with_groups) > 0:
        continue

    ns = project.namespace
    if ns['kind'] == 'group' and ns['id'] not in seen_groups:
        seen_groups.append(ns['id'])
        result.extend(get_group_info(gl, ns['id']))

    members = project.members.list(get_all=True)
    info = get_members_info(members, 'project', project.id, project.name)
    result.extend(info)

file_name = (f'{main_group.name.replace(" ", "-")}_members_' +
             f'{datetime.date.today()}.csv')
with open(file_name, 'w') as outfile:
    fieldnames = ['kind', 'id', 'name', 'member', 'level', 'granted_by']
    writer = csv.DictWriter(outfile, fieldnames)
    writer.writeheader()
    writer.writerows(result)

print(f'This script took {round(time.time() - start_time, 1)}s')
print(f'CSV file written to {file_name}')
