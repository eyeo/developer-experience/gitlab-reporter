import argparse
from utils import get_gitlab
import datetime

parser = argparse.ArgumentParser('gitlab_minutes')
parser.add_argument('id', help='Gitlab project id', type=str)
parser.add_argument('token', help='Gitlab access token', type=str)
parser.add_argument('--no_limit', action='store_true',
                    help='Removes the default limit of one month for ' +
                         'pipelines data')
args = parser.parse_args()

# By default the script will not process pipelines older than one month

today = datetime.date.today()
one_month_ago = (today.replace(day=1) - datetime.timedelta(days=1)).replace(
                day=today.day)

print('authenticating to Gitlab...')
gl = get_gitlab(args.token)
project = gl.projects.get(args.id)

print(f'getting pipelines data for the "{project.name}" project...')
pipelines = project.pipelines.list(get_all=True)
day = None
day_minutes = 0
for project_pipeline in pipelines:
    pipeline = project.pipelines.get(project_pipeline.id)
    pipeline_minutes = (0 if pipeline.duration is None
                        else round(pipeline.duration / 60, 2))

    jobs = pipeline.jobs.list()
    jobs_duration = 0
    for job in jobs:
        if job.duration is not None:
            jobs_duration += job.duration
    jobs_minutes = round(jobs_duration / 60, 2)

    current_day = pipeline.created_at.split('T')[0]
    if current_day == day:
        day_minutes += jobs_minutes
    else:
        if day is not None:
            print(f'{day} TOTAL JOB MINUTES: {round(day_minutes, 2)}')
        if not args.no_limit:
            pipeline_date = datetime.datetime.fromisoformat(current_day).date()
            if pipeline_date < one_month_ago:
                break
        day = current_day
        day_minutes = jobs_minutes

    print(f'pipeline: {pipeline.id}, created: {pipeline.created_at}, ' +
          f'finished: {pipeline.finished_at}, minutes: {pipeline_minutes}, ' +
          f'jobs count: {len(jobs)}, jobs minutes: {jobs_minutes}')
