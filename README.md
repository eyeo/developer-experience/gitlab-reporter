# gitlab-reporter

Scripts to query the Gitlab API and make reports.

## Getting started

### Prerequisites

- Python >= 3.6

### Installing dependencies

```shell
pip install -r requirements.txt
```

### Running the scripts

```shell
python gitlab_members.py <GROUP_ID> <ACCESS_TOKEN> [--admin_only]
python gitlab_minutes.py <PROJECT_ID> <ACCESS_TOKEN> [--no_limit]
```

## Development

### Installing dev dependencies

```shell
pip install -r requirements-dev.txt
```

### Running the linter

```shell
flake8
```
