import gitlab


def get_gitlab(private_token):
    return gitlab.Gitlab('https://gitlab.com', private_token, per_page=100)
